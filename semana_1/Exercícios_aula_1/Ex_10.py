'''
Exercício #10
Faça um programa que leia 5 números e informe o maior número.
'''

contador = 0
maiorNumero = 0

while contador < 5:
    numeroDigitado = float(input('Entre com um número: '))
    if numeroDigitado > maiorNumero:
        maiorNumero = numeroDigitado

    contador = contador + 1

print('O maior número digitado foi: {}'.format(maiorNumero))