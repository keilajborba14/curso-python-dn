'''
Problema #2
Faça um programa que pergunte o nome e a idade do usuário e o exiba na tela.
“Bem vindo <nome>, fico feliz em saber que tem <idade> anos”.
'''

nome = input('Qual é o seu nome?')
idade = int(input('Qual é a sua idade (anos?): '))

print('Bem-vindo(a), {}, fico feliz em saber que tem {} anos.'.format(nome,idade))