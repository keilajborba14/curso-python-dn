'''

Faça um programa, com uma função, que calcule a dispersão de uma lista
Funções embutidas que podem te ajudar:
● min(lista) -> retorna o menor valor
● max(lista) -> retorna o maior valor

'''

lista = [10, 20, 40, 60, 50]

def dispersao(list):
    menorValor = min(lista)
    maiorValor = max(lista)

    return (maiorValor - menorValor)

print(dispersao(lista))
