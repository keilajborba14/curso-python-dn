'''
Exercício #7
Faça um programa que receba uma string e responda se ela tem alguma vogal,
se sim, quais são? E também diga se ela é uma frase ou não.
'''

palavra = input('Entre com uma palavra: ')

if 'a' in palavra or 'e' in palavra or 'i' in palavra or 'o' in palavra or 'u' in palavra:
    print('A palavra {} contém vogal(s)'.format(palavra))

else:
    print('A palavra digitada não contém vogal(s)')

if 'a' in palavra:
    print('A palavra {} contém a vogal "A"'.format(palavra))

if 'e' in palavra:
    print('A palavra {} contém a vogal "E"'.format(palavra))

if 'i' in palavra:
    print('A palavra {} contém a vogal "I"'.format(palavra))

if 'o' in palavra:
    print('A palavra {} contém a vogal "O"'.format(palavra))

if 'u' in palavra:
    print('A palavra {} contém a vogal "U"'.format(palavra))
    
    

if ' ' in palavra:
    print('Foi digitado uma frase.')