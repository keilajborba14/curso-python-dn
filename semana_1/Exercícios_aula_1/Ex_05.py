'''
Exercício #5
Faça um programa para a leitura de duas notas parciais de um aluno. O programa
deve calcular a média alcançada por aluno e apresentar:

    1. A mensagem "Aprovado", se a média alcançada for maior ou igual a sete;
    2. A mensagem "Reprovado", se a média for menor do que sete;
    3. A mensagem "Aprovado com Distinção", se a média for igual a dez.
'''


nota1 = int(input('Entre com a primeira nota: '))
nota2 = int(input('Entre com a segunda nota: '))

media = (nota1 + nota2) / 2

if media == 10:
    print('APROVADO COM DISTIÇÃO!')

elif media >= 7 and media < 10:
    print('APROVADO!')
else:
    print('REPROVADO!')