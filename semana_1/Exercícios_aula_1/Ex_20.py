'''

Baseando-se nos exercícios passados, monte um dicionário com os seguintes
seguintes chaves:
lista, somatório, tamanho, maior valor e menor valor

'''

lista = [10, 20, 30, 40, 50]

dicionario = {}

def dicionario(list):

    soma = sum(lista)
    tamanho = len(lista)
    maior = max(lista)
    menor = min(lista)
    
    return (f'A soma é da lista é: {soma}, o tamanho é dela é: {tamanho}, o maior número é o :{maior} e o menor número é o: {menor}')

print(dicionario(lista))
