'''
Entre com dois números inteiros e um número real.

O produto do dobro do primeiro com metade do segundo
A soma do triplo do primeiro com o terceiro
O terceiro elevado ao cubo
'''

numero1 = int(input('Digite um número inteiro: '))
numero2 = int(input('Digite o segundo inteiro: '))
numero3 = float(input('Digite um número real: '))

caso1 = (numero1 * 2) + (numero2 / 2)
print('O produto do dobro do primeiro com metade do segundo é: {}'.format(caso1))

caso2 = (numero1*3) + numero3
print('A soma do triplo do primeiro com o terceiro é: {}'.format(caso2))

caso3 = numero3**3
print('O terceiro elevado ao cubo é: {}'.format(caso3))
