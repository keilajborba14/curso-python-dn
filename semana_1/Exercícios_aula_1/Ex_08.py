"""
Exercício #8
Faça um programa que receba uma data de nascimento (15/07/87) e imprima
'Você nasceu em <dia> de <mes> de <ano>'
"""

nascimento = input('Entre com a sua data de nascimento no formato (dd/mm/aa): ')

dia, mes, ano = nascimento.split('/')
print(' Você nasceu no dia {}, no mês {} e no ano 19{}.'.format(dia, mes, ano))