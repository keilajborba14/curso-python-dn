'''
Exercício # 12
Faça um programa que receba uma string, com um número de ponto flutuante, e
imprima qual a parte dele que não é inteira
EX:
n = ‘3.14’
responsta: 14
'''

numero_real = str(input('Entre com um número real: '))

numero_final = numero_real.split('.')[1]

print('A parte real do número é: "{}"'.format(numero_final))