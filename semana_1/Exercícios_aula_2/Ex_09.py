'''

Problema #9
Crie uma função que faça uma saudação a alguém. A função deve receber dois
argumentos ‘saudação’ e ‘nome’.
Ex:
f(‘Ahoy’, ‘Fausto’) -> ‘Ahoy Fausto’
f(‘Olá’, bb’) -> Olá bb’

'''

nome = input("Entre com seu nome: ")

def saudar(saudacao, nome):
    return saudacao, nome

print(saudar('Hello', nome))
print(saudar('Olá', 'bb'))
