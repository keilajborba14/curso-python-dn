'''
Dada que exista uma função de soma e uma de subtração.
Crie uma função chamada ‘exp’ que receba x, y e z. 
E calcule: f(x, y, z) -> x + y - z
Faça o teste dessa função, testando seus inputs e outputs indiretos
'''

from unittest import TestCase, main

def soma(num1, num2):
    return num1 + num2

def subtracao(num1, num2):
    return num1 - num2

def exp(num1, num2, num3):
    return subtracao(soma(num1, num2), num3)

class TesteExpressao(TestCase):
    def testar_expressao(self):
        self.assertEqual(exp(10, 20, 5),25)
        