'''
Agora que você já contruiu a função ‘exp’
que tal fazer uma chamada de uma expressão mais complexa?
f(x, y, z) -> exp(exp(x, y, z), y, z)
Agora tente efetuar os testes dessa função
'''

from unittest import TestCase, main

def soma(num1, num2):
    return num1 + num2

def subtracao(num1, num2):
    return num1 - num2

def exp(num1, num2, num3):
    return subtracao(soma(num1, num2), num3)

def expressao_nova(num1, num2, num3):
    return exp(exp(num1, num2, num3), num2, num3)


class TesteExpressao(TestCase):

    def teste_1(self):
        self.assertEqual(expressao_nova(5, 6, 1),15)
        # 5 + 6 = 11 / 11 - 1 = 10 / 10 + 6 = 16 / 16 - 1 = 15

    def teste_2(self):
        self.assertEqual(expressao_nova(2, 4, 3),4)
        # 2 + 4 = 6 / 6 - 3 = 3 / 3 + 4 = 7 / 7 - 3 = 4
        
    def teste_3(self):
        self.assertEqual(expressao_nova(4, 5, 7),0)
        # 4 + 5 = 9 / 9 - 7 = 2 / 2 + 5 = 7 / 7 - 7 = 0