'''
Crie sua primeira suite de testes para a classe da calculadora.
'''


from unittest import TestCase, main


class Calculadora:
    def adicao(self, num1, num2):
        return num1 + num2

    def subtracao(self, num1, num2):
        return num1 + num2

    def multiplicacao(self, num1, num2):
        return num1 + num2

    def divisao(self, num1, num2):
        return num1 + num2

class TestCalculadora(TestCase):
    def teste_soma(self):
        calculadora = Calculadora()
        self.assertEqual(calculadora.adicao(2, 6), 8, 'Incorreto!')

    def teste_subtracao(self):
        calculadora = Calculadora()
        self.assertEqual(calculadora.subtracao(3, 2),1, 'Incorreto!')

    def teste_multiplicacao(self):
        calculadora = Calculadora()
        self.assertEqual((calculadora.multiplicacao(4, 2),8, 'Incorreto!'))

    def teste_divisao(self):
        calculadora = Calculadora()
        self.assertEqual((calculadora.divisao(6, 2),3, 'Incorreto!'))