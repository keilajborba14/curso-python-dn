'''
Dada a função soma:
def soma(x, y):
    return x + y
como ela pode ser testada?
'''

def soma(x, y):
    return x + y

assert soma(1, 2) == 2
assert soma(2, 3) == 5

