''
Dada uma função de soma:
 def soma(x, y):
    return x + y
Faça assertivas de valores que você acredita serem coerentes com a função
'''

def soma(x, y):
    return x + y

assert soma(1, 4) == 5
assert soma(2, 4) == 6
assert soma(3, 5) == 8