'''
Dada a classe da calculadora. Defina difentes SUTs e os teste individualmente.
'''

from unittest import TestCase, main


class Calculadora:
    def adicao(self, num1, num2):
        return num1 + num2

    def subtracao(self, num1, num2):
        return num1 + num2

    def multiplicacao(self, num1, num2):
        return num1 + num2

    def divisao(self, num1, num2):
        return num1 + num2

class TestCalculadora(TestCase):
    def teste_soma(self):
        calculadora = Calculadora()
        self.assertEqual(calculadora.adicao(2, 6), 8, 'Incorreto!')
        self.assertEqual(calculadora.adicao(5, 2), 7, 'Incorreto!')

    def teste_subtracao(self):
        calculadora = Calculadora()
        self.assertEqual(calculadora.subtracao(3, 2),1, 'Incorreto!')
        self.assertEqual(calculadora.subtracao(10, 4),6, 'Incorreto!')

    def teste_multiplicacao(self):
        calculadora = Calculadora()
        self.assertEqual((calculadora.multiplicacao(4, 2),8, 'Incorreto!'))
        self.assertEqual((calculadora.multiplicacao(8, 2), 16, 'Incorreto!'))

    def teste_divisao(self):
        calculadora = Calculadora()
        self.assertEqual((calculadora.divisao(6, 2),3, 'Incorreto!'))
        self.assertEqual((calculadora.divisao(15, 3), 5, 'Incorreto!'))